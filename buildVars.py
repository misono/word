# Build customizations
# Change this file instead of sconstruct or manifest files, whenever possible.

# Full getext (please don't change)
_ = lambda x : x

# Add-on information variables
addon_info = {
	# add-on Name
	"addon-name" : "word",
	# Add-on description
	# TRANSLATORS: Summary for this add-on to be shown on installation and add-on information.
	"addon-summary" : _("Extended functionality for Microsoft Word"),
	# Add-on description
	# Translators: Long description to be shown for this add-on on installation and add-on information
	"addon-description" : _("""This add-on adds extra functionality when working with Microsoft Word.
For now it just adds the ability to announce the text of a comment with NVDA+shift+c.
Suggestions for additional features are welcome."""),
	# version
	"addon-version" : "1.0-dev",
	# Author(s)
	"addon-author" : "Bram Duvigneau <bram@bramd.nl>, Masamitsu Misono <misono@nvsupport.org> and other NVDA contributors",
	# URL for the add-on documentation support
	"addon-url" : "http://addons.nvda-project.org"
}


import os.path

# Define the python files that are the sources of your add-on.
# You can use glob expressions here, they will be expanded.
pythonSources = ['addon/appModules/*.py']

# Files that contain strings for translation. Usually your python sources
i18nSources = pythonSources + ["buildVars.py"]

# Files that will be ignored when building the nvda-addon file
# Paths are relative to the addon directory, not to the root directory of your addon sources.
excludedFiles = []
