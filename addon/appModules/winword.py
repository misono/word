import appModuleHandler
import addonHandler
import api
import ui
from logHandler import log
addonHandler.initTranslation()
import scriptHandler


#Indexing
wdActiveEndAdjustedPageNumber=1
wdActiveEndPageNumber=3
wdNumberOfPagesInDocument=4
wdHorizontalPositionRelativeToPage=5
wdVerticalPositionRelativeToPage=6
wdHorizontalPositionRelativeToTextBoundary=7
wdVerticalPositionRelativeToTextBoundary=8
wdFirstCharacterColumnNumber=9
wdFirstCharacterLineNumber=10
wdFrameIsSelected=11
wdWithInTable=12
wdStartOfRangeRowNumber=13
wdEndOfRangeRowNumber=14
wdMaximumNumberOfRows=15
wdStartOfRangeColumnNumber=16
wdEndOfRangeColumnNumber=17
wdMaximumNumberOfColumns=18
wdZoomPercentage=19
wdSelectionMode=20
wdCapsLock=21
wdNumLock=22
wdOverType=23
wdRevisionMarking=24
wdInFootnoteEndnotePane=25
wdInCommentPane=26
wdInHeaderFooter=28
wdAtEndOfRowMarker=31
wdReferenceOfType=32
wdHeaderFooterType=33
wdInMasterDocument=34
wdInFootnote=35
wdInEndnote=36
wdInWordMail=37
wdInClipboard=38


# Map revision types to human readable names. Just the basics for now, full list at:
# http://msdn.microsoft.com/en-us/library/office/ff839110.aspx
REVISION_TYPES = {
    # Translators: A type of revision
    0: _("No revision"),
    # Translators: A type of revision
    1: _("Insertion"),
    # Translators: A type of revision
    2: _("Deletion"),
}

class AppModule(appModuleHandler.AppModule):
    
    def _get_comments(self, doc):
        comments = []
        for i in range(doc.Comments.Count):
            comment = doc.Comments[i]
            comments.append((comment.Range.Text, comment.Scope.Start, comment.Scope.End))
        return comments
    
    def _get_revisions(self, doc):
        revisions = []
        for i in range(doc.Revisions.Count):
            revision = doc.Revisions[i]
            # Translators: Description for a revision
            description = _("{revisionType} by {author}").format(
                # Translators: Unknown revision type
                revisionType = REVISION_TYPES.get(revision.Type, _("Unknown")),
                author = revision.Author
            )
            revisions.append((description, revision.Range.Start, revision.Range.End))
        log.debug(revisions)
        return revisions
    
    def script_report_comment(self, gesture):
        focus = api.getFocusObject()
        doc = getattr(focus, "WinwordDocumentObject", None)
        selection = getattr(focus, "WinwordSelectionObject", None)
        if not doc:
            # Translators: Message presented when user is trying to read comments from programs other than Microsoft Word, specifically from somewhere other than word document.
            ui.message(_("Not in a Word document."))
            return False
        comments = self._get_comments(doc)
        selected_range = set(range(selection.Start, selection.End + 1))
        messages = []
        
        for comment in comments:
            if selected_range.intersection(range(comment[1], comment[2] + 1)):
                messages.append(comment[0])
        # Translators: The user is in a Word document, but there are no comments at the 
        # position of the caret.
        ui.message(', '.join(messages) or _('No comments at current position'))
        return True
    # Translators: presented in input help mode to describe functionality of the keypress..
    script_report_comment.__doc__ = _("Presents comments in a word document.")

    def script_report_revision(self, gesture):
        focus = api.getFocusObject()
        doc = getattr(focus, "WinwordDocumentObject", None)
        selection = getattr(focus, "WinwordSelectionObject", None)
        if not doc:
            # Translators: Message presented when user is trying to read revisions from programs other than Microsoft Word, specifically from somewhere other than word document.
            ui.message(_("Not in a Word document."))
            return False
        revisions = self._get_revisions(doc)
        selected_range = set(range(selection.Start, selection.End + 1))
        messages = []
        
        for revision in revisions:
            if selected_range.intersection(range(revision[1], revision[2] + 1)):
                messages.append(revision[0])
        # Translators: The user is in a Word document, but there is no revision at the 
        # position of the caret or selection.
        ui.message(', '.join(messages) or _('No revision at current position'))
        return True
    # Translators: presented in input help mode to describe functionality of the keypress..
    script_report_revision.__doc__ = _("Presents revisions in a word document.")


    def script_createGeneral(self, gesture):
        if scriptHandler.getLastScriptRepeatCount() == 2:
            self._reportPageSetting()
        elif scriptHandler.getLastScriptRepeatCount() == 1:
            self._reportTotal()
        else:
            self._reportCurrent()

    def script_createDitail(self, gesture):
        if scriptHandler.getLastScriptRepeatCount() == 1:
            self._reportPosition()
        else:
            self._reportTable()

    def _reportCurrent(self):
        focus = api.getFocusObject()
        doc = getattr(focus, "WinwordDocumentObject", None)
        selection = getattr(focus, "WinwordSelectionObject", None)
        if not doc:
            ui.message(_("Not in a Word document."))
            return False
        ui.message(_("current  "))
        if selection.Information(wdWithInTable):
            curRowIndex = selection.Information(wdStartOfRangeRowNumber)
            curColumnIndex = selection.Information(wdStartOfRangeColumnNumber)
            ui.message(_("rows %s  columns %s in a table") % (curRowIndex,curColumnIndex))
        if selection.Information(wdInCommentPane):
            ui.message(_("in a comment"))
        if selection.Information(wdInHeaderFooter):
            ui.message(_("in a header or footer"))
        if selection.Information(wdAtEndOfRowMarker):
            ui.message(_("contains a marker"))
        curPgIndex = selection.Information(wdActiveEndPageNumber)
        firstPgIndex = selection.Information(wdActiveEndAdjustedPageNumber)
        curLnIndex = selection.Information(wdFirstCharacterLineNumber)
        curChIndex = selection.Information(wdFirstCharacterColumnNumber)
        ui.message(_("pages %s  lines %s  characters %s") % (curPgIndex,curLnIndex,curChIndex))

    def _reportTotal(self):
        focus = api.getFocusObject()
        doc = getattr(focus, "WinwordDocumentObject", None)
        selection = getattr(focus, "WinwordSelectionObject", None)
        if not doc:
            ui.message(_("Not in a Word document."))
            return False
        totalPg = selection.Information(wdNumberOfPagesInDocument)
        totalTable = doc.Tables.Count
        totalInlineShape = doc.InlineShapes.Count
        #totalShape = doc.Shapes.Count
        ui.message(_("total page %s  table %s  shape %s") % (totalPg,totalTable,totalInlineShape))

    def _reportPageSetting(self):
        focus = api.getFocusObject()
        doc = getattr(focus, "WinwordDocumentObject", None)
        selection = getattr(focus, "WinwordSelectionObject", None)
        if not doc:
            ui.message(_("Not in a Word document."))
            return False
        setLn = doc.PageSetup.LinesPage
        setCh = doc.PageSetup.CharsLine
        # paperSize = doc.PageSetup.PaperSize
        ui.message(_("settings lines %s  characters %s in a page") % (setLn,setCh))

    def _reportTable(self):
        focus = api.getFocusObject()
        doc = getattr(focus, "WinwordDocumentObject", None)
        selection = getattr(focus, "WinwordSelectionObject", None)
        if not doc:
            ui.message(_("Not in a Word document."))
            return False
        isTable = selection.Information(wdWithInTable)
        if isTable:
            totalRowsIndex = selection.Information(wdMaximumNumberOfRows)
            totalColumnsIndex = selection.Information(wdMaximumNumberOfColumns)
            ui.message(_("total rows %s  columns %s") % (totalRowsIndex,totalColumnsIndex))
        else:
            ui.message(_("not table"))

    def _reportPosition(self):
        focus = api.getFocusObject()
        doc = getattr(focus, "WinwordDocumentObject", None)
        selection = getattr(focus, "WinwordSelectionObject", None)
        if not doc:
            ui.message(_("Not in a Word document."))
            return False
        horizontalPgPos = selection.Information(wdHorizontalPositionRelativeToPage)
        verticalPgPos = selection.Information(wdVerticalPositionRelativeToPage)
        horizontalTxPos = selection.Information(wdHorizontalPositionRelativeToTextBoundary)
        verticalTxPos = selection.Information(wdVerticalPositionRelativeToTextBoundary)
        zoomPer = selection.Information(wdZoomPercentage)
        ui.message(_("position horizontal %s  vertical %s in a page,  horizontal %s  vertical %s in a document  zoom %s%%") % (horizontalPgPos,verticalPgPos,horizontalTxPos,verticalTxPos,zoomPer))


    __gestures = {
        "kb:nvda+shift+c": "report_comment",
        "kb:nvda+shift+r": "report_revision",
        "kb:F9": "createGeneral",
        "kb:shift+F9": "createDitail",
    }
